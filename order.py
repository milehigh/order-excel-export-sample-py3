import openpyxl
from openpyxl import load_workbook
import sqlite3

print("受注番号を入力してください")
input_order_id = input()

dbname = "sample.db"

# データベースに接続
conn = sqlite3.connect(dbname)

# データベースを操作するためのカーソル（リモコンのようなもの）を取得する
cur = conn.cursor()

# SQL を実行
cur.execute("SELECT\
    orders.created, users.name, users.address, users.phone\
    FROM orders\
    JOIN users ON orders.user_id = users.id\
    WHERE orders.id=:order_id",
{'order_id': input_order_id})
order = cur.fetchone()

cur.execute("SELECT items.name, items.price, order_details.amount\
    FROM order_details\
    JOIN items ON order_details.item_id = items.id\
    WHERE order_details.order_id=:order_id",
{'order_id': input_order_id})
order_details = cur.fetchall()

# 不要になったカーソルをクローズ
cur.close()

# データベースとの接続を切る
conn.close()

# エクセルを開く
workbook = load_workbook('order_template.xlsx')
sheet = workbook['order']

# 共通情報を設定
order_id_format = input_order_id.zfill(8)
sheet['B4'].value = order[1] + ' 様'    #name
sheet['B5'].value = order[2]            #address
sheet['B9'].value = order[3]            #price
sheet['N4'].value = order_id_format
sheet['N5'].value = order[0]            #created

# 商品ごとの情報を設定
row_base = 16
for order_detail in order_details:
    sheet['B' + str(row_base)].value = order_detail[0]  #name
    sheet['J' + str(row_base)].value = order_detail[2]  #amount
    sheet['L' + str(row_base)].value = order_detail[1]  #price
    row_base += 1
    if (row_base >= 25):
        break

workbook.save(order_id_format + ".xlsx")
