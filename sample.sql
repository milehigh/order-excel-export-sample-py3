PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "items" (
	"id"	INTEGER,
	"name"	TEXT,
	"price"	INTEGER,
	PRIMARY KEY("id")
);
INSERT INTO items VALUES(1,'コーラ',100);
INSERT INTO items VALUES(2,'USBメモリ',2000);
INSERT INTO items VALUES(3,'傘',500);
INSERT INTO items VALUES(4,'お茶',100);
INSERT INTO items VALUES(5,'麦茶',100);
INSERT INTO items VALUES(6,'烏龍茶',120);
INSERT INTO items VALUES(7,'コーヒー',200);
INSERT INTO items VALUES(8,'メモ帳',600);
CREATE TABLE IF NOT EXISTS "orders" (
	"id"	INTEGER,
	"user_id"	INTEGER,
	"created"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO orders VALUES(1,1,'2020-05-27 12:22:34');
INSERT INTO orders VALUES(2,2,'2020-05-28 12:22:34');
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER,
	"name"	TEXT,
	"address"	TEXT,
	"phone"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO users VALUES(1,'山田太郎','東京都港区六本木','03-1234-5678');
INSERT INTO users VALUES(2,'鈴木誠','神奈川県横浜市中区','090-9999-1111');
CREATE TABLE IF NOT EXISTS "order_details" (
	"id"	INTEGER,
	"order_id"	INTEGER,
	"item_id"	INTEGER,
	"amount"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO order_details VALUES(1,1,1,3);
INSERT INTO order_details VALUES(2,1,2,1);
INSERT INTO order_details VALUES(3,1,3,4);
INSERT INTO order_details VALUES(4,2,1,1);
INSERT INTO order_details VALUES(5,2,2,1);
INSERT INTO order_details VALUES(6,2,4,4);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('orders',2);
INSERT INTO sqlite_sequence VALUES('users',2);
INSERT INTO sqlite_sequence VALUES('order_details',6);
COMMIT;
